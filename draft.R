## How to create a R package
## https://www.youtube.com/watch?v=MoszELQFrvQ

install.packages(c("aqp", "bayestestR", "bbmle", "cNORM", "credentials", "e1071", 
                   "echarts4r", "fastmatch", "ggmulti", "gss", "hardhat", 
                   "HMMextra0s", "jpeg", "matrixcalc", "maxLik", "mlr3misc", 
                   "modeldata", "modeltime", "paletteer", "parsnip", "performance", 
                   "phangorn", "pkgKitten", "posterior", "pryr", "ranger", 
                   "rbibutils", "rbokeh", "RcppAnnoy", "RcppArmadillo", 
                   "RcppCWB", "RcppSpdlog", "renv", "rgl", "rsconnect", 
                   "Rttf2pt1", "shinydashboardPlus", "shinyMobile", 
                   "stringdist", "targets", "testthat", "tune", "tzdb", 
                   "vroom", "waiter", "workflows", "workflowsets", "x13binary", 
                   "countrycode", "echarts4r", "foghorn", "IRdisplay", "IRkernel", 
                   "MNLpred", "paletteer", "pbdZMQ", "reprex", "rsconnect", 
                   "showtext", "sysfonts", "tsBSS"), 
                 lib='/usr/lib/R/library', dependencies = TRUE, 
                 'https://cloud.r-project.org', 
                 INSTALL_opts=c('--no-multiarch', '--no-lock'))

install.packages(c("aqp", "bayestestR", "cNORM", "credentials", "e1071", 
                   "echarts4r", "fastmatch", "ggmulti", "gss", "hardhat", 
                   "HMMextra0s", "jpeg", "matrixcalc", "maxLik", "mlr3misc", 
                   "modeldata", "modeltime", "paletteer", "parsnip", 
                   "performance", "phangorn", "pkgKitten", "posterior", 
                   "pryr", "ranger", "rbibutils", "rbokeh", "RcppAnnoy", 
                   "RcppArmadillo", "RcppCWB", "RcppSpdlog", "renv", 
                   "reprex", "rgl", "rsconnect", "Rttf2pt1", "shinydashboardPlus", 
                   "shinyMobile", "stringdist", "targets", "testthat", "tune", 
                   "tzdb", "vroom", "waiter", "workflows", "workflowsets", 
                   "x13binary", "countrycode", "echarts4r", "foghorn", 
                   "IRdisplay", "IRkernel", "MNLpred", "paletteer", "pbdZMQ", 
                   "reprex", "rsconnect", "showtext", "sysfonts", "tsBSS"), 
                 lib = '/usr/lib/R/library', 'https://cloud.r-project.org', 
                 dependencies = TRUE, INSTALL_opts = '--no-lock')

install.packages(
  c('xfun', 'tzdb', 'voom', 'rmarkdown', 'Rcpp', 'reprex'), 
  lib = '/usr/lib/R/library', 'https://cloud.r-project.org', 
  dependencies = TRUE, INSTALL_opts = '--no-lock')

